<h2>コーディング規約</h2>
<ul>
   <li>案件ごとにフォルダを作成し、その中にhtmlファイルを格納すること</li>
   <li>基本的なCSSはcss/common.cssに格納されています</li>
   <li>自らCSSを加える場合はhtmlファイルの &lt;style&gt; 〜 &lt;/style&gt; の中に書くこと</li>
   <li style="color: red;">common.cssは全ての大元のCSSのため絶対にいじらないこと</li>
   <li>common.cssの中身はBootstrap4のクラス名に準拠しています。よって、使用するクラス名はBootstrapのリファレンス(https://getbootstrap.jp/docs/4.3/getting-started/introduction/)を参考にすること</li>
   <li>画像等は基本的には使わないのでimgフォルダは存在しません</li>
   <li>JavaScript(jQuery)が必要な場合は、htmlファイル内下部のscriptタグ内に記入する</li>
</ul>


<h2>gitでの開発フロー</h2>
<p>推奨環境(ローカル)：github desktop</p>
<p>github flow(参考：https://thinkit.co.jp/article/8410)に準拠</p>

<ul>
   <li>新しい案件に着手する際は、masterではなく作業ブランチを作成してそこで作業すること（ブランチ名は案件名などわかりやすいものにする）</li>
   <li>作業完了後、masterへマージリクエスト</li>
   <li>霧生がレビューしてOKであればマージ</li>
   <li>修正点がある場合はマージせず修正して再度マージリクエスト</li>
   <li>↑OKまで繰り返し</li>
   <li>マージ後、以下の納品方法に従って納品</li>
</ul>


<h2>納品方法</h2>
<ul>
   <li>この案件では、htmlファイルだけをクライアントに送る必要があります（CSSファイルやimgフォルダ等は含まない）</li>
   <li>よって、現在読み込まれているcommon.cssを全選択してhtmlファイルの &lt;style&gt; 〜 &lt;/style&gt; 内に貼り付けてから送ります</li>
   <li>head内のcss読み込みを削除します</li>
   <li>ファイルが複数ある場合は上の作業を全てのhtmlファイルで行い、案件のフォルダごとzip化して送ります</li>
</ul>
